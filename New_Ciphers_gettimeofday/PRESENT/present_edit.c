#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
/*
 *
 * The input is from right to left. 
 * The rightmost byte is the least significant byte.
 * The leftmost byte is the most significant byte.
 */


#define PRESENT_BLOCK_SIZE   (64)  /* block size in bits */
//#define PRESENT_KEY_SIZE_80  (80)  /* key size in bits */
#define PRESENT_KEY_SIZE_128 (128) /* key size in bits */
#define PRESENT_ROUNDS       (31)  /* rounds */
double standard_deviation(long double timearray[], int n, long double timeavg);
/*
 * rotate shift right n-bit on x(a 64-bit block)
 */
static inline uint64_t ror64(uint64_t x, int n) { return x>>n | x<<(64-n); }

/*
 * rotate shift left x by n bits
 */
static inline uint32_t rol32(uint32_t x, int n) { return x<<n | x>>(32-n); }

/*
 * rotate shift right x by n bits
 */
static inline uint32_t ror32(uint32_t x, int n) { return x>>n | x<<(32-n); }
typedef  uint8_t  u8;
typedef  uint16_t u16;
typedef  uint32_t u32;
typedef  uint64_t u64;

/*
 * key schedule
 * inputKey: the original keys
 * keys: round keys
 */
//void present_64_80_key_schedule(const u8 * inputKey, u8 * keys );

void present_64_128_key_schedule(const u8 * inputKey, u8 * keys );

/*
 * encrypt
 * plainText: plainText has just one block.
 * keys: round keys
 */
void present_encrypt(u8 * plainText, const u8 * keys );

/*
 * decrypt
 * cipherText: cipherText has just one block.
 * keys: round keys
 */
//void present_decrypt(u8 * cipherText, const u8 * keys );



/*
 * PRESENT_64_80
 */

static u8 sbox[] = {0xc, 0x5, 0x6, 0xb, 0x9, 0x0, 0xa, 0xd, 0x3, 0xe, 0xf, 0x8, 0x4, 0x7, 0x1, 0x2};
static u8 invsbox[] = {0x5, 0xe, 0xf, 0x8, 0xC, 0x1, 0x2, 0xD, 0xB, 0x4, 0x6, 0x3, 0x0, 0x7, 0x9, 0xA};

/*
 * Key schedule for 80-bit
 * key: master key
 * roundKeys: round keys
 */
/*void present_64_80_key_schedule( const u8 *key, u8 *roundKeys) {
	u64 keylow = *(const u64*)key;
	u16 highBytes = *(const u16*)(key + 8);
	u64 keyhigh = ((u64)(highBytes) << 48) | (keylow >> 16);
	u64 *rk = (u64*)roundKeys;
	rk[0] = keyhigh;

	u64 temp;
	u8 i;
	for (i = 0; i < PRESENT_ROUNDS; i++) {
		// 61-bit left shift 
		temp = keyhigh;
		keyhigh <<= 61;
		keyhigh |= (keylow << 45);
		keyhigh |= (temp >> 19);
		keylow = (temp >> 3) & 0xFFFF;

		// S-Box application 
		temp = sbox[keyhigh >> 60];
		keyhigh &= 0x0FFFFFFFFFFFFFFF;
		keyhigh |= temp << 60;

		// round counter addition 
		keylow ^= (((u64)(i + 1) & 0x01) << 15);
		keyhigh ^= ((u64)(i + 1) >> 1);

		rk[i+1] = keyhigh;
	}
}
*/
/*
 * Key schedule for 128-bit
 * key: master key
 * roundKeys: round keys
 */
void present_64_128_key_schedule( const u8 *key, u8 *roundKeys) {
	u64 keylow = *(const u64*)key;
	u64 keyhigh = *((const u64*)key+1);
	u64 *rk = (u64*)roundKeys;
	rk[0] = keyhigh;

	u64 temp;
	u8 i;

	for (i = 0; i < PRESENT_ROUNDS; i++) {
		/* 61-bit left shift */
		temp = ( (keyhigh<<61) | (keylow>>3) );
		keylow = ( (keylow<<61) | (keyhigh>>3) );
		keyhigh = temp;

		/* S-Box application */
		temp = (sbox[keyhigh>>60]<<4) ^ (sbox[(keyhigh>>56)&0xf]);
		keyhigh &= 0x00FFFFFFFFFFFFFF;
		keyhigh |= temp<<56;

		/* round counter addition */
		temp = ((keyhigh<<2) | (keylow>>62)) ^ (u64)(i + 1);
		keyhigh = (keyhigh & 0xFFFFFFFFFFFFFFF8) ^ (temp & 0x7);
		keylow = (keylow & 0x3FFFFFFFFFFFFFFF) ^ (temp << 62);

		rk[i+1] = keyhigh;
	}
}

/*
 * one block is encrypted
 * plainText: one block of plain text
 * roundKeys: round keys
 *
 */
void present_encrypt(u8 *plainText, const u8 *roundKeys) {
	u64 state = *(u64*)plainText;
	const u64* rk = (const u64*)roundKeys;
	u64 result;
	u8 sInput; // every nibble of sbox
	u8 pLayerIndex; // the output position of every bit in pLayer
	u64 stateBit; // the input value of every bit in pLayer
	u8 i; // rounds
	u16 k;
	
	for (i = 0; i < PRESENT_ROUNDS; i++) {		
		state ^= rk[i];

		/* sbox */
		for (k = 0; k < PRESENT_BLOCK_SIZE/4; k++) {
			sInput = state & 0xF;
			state &= 0xFFFFFFFFFFFFFFF0; 
			state |= sbox[sInput];
			state = ror64(state, 4); 
		}
		
		/* pLayer */
		result = 0;
		for (k = 0; k < PRESENT_BLOCK_SIZE; k++) {
			stateBit = state & 0x1;
			state = state >> 1;
			if ( 0 != stateBit ) {
				pLayerIndex = (16 * k) % 63;
				if (63 == k) {
					pLayerIndex = 63;
				}
				/*
				 * result |= 0x1 << pLayerIndex;
				 * 0x1 is 32-bit by default
				 */
				result |= stateBit << pLayerIndex; 
			}
		}
		state = result;
	}

	state ^= rk[i];
	*(u64*)plainText = state;
}

/*
 * one block is decrypted
 * cipherText: one block of cipher text
 * roundKeys: round keys
 */
/*
void present_decrypt(u8 *cipherText, const u8 *roundKeys) {
	u64 state = *(u64*)cipherText;
	const u64* rk = (const u64*)roundKeys;
	u64 result;
	u8 sInput; // every nibble of sbox
	u8 pLayerIndex; // the output position of every bit in pLayer
	u64 stateBit; // the input value of every bit in pLayer
	u8 i; // rounds
	u16 k;
	
	for (i = PRESENT_ROUNDS; i > 0; i--){
		state ^= rk[i];

		// pLayer 
		result = 0;
		for (k = 0; k < PRESENT_BLOCK_SIZE; k++) {
			stateBit = state & 0x1;
			state = state >> 1;
			if ( 0 != stateBit ) {
				pLayerIndex = (4 * k) % 63;
				if (63 == k) {										
					pLayerIndex = 63;
				}
				result |= stateBit << pLayerIndex; 
			}
		}
		state = result;

		// sbox 
		for (k = 0; k < PRESENT_BLOCK_SIZE/4; k++) {
			sInput = state & 0xF;
			state &= 0xFFFFFFFFFFFFFFF0; 
			state |= invsbox[sInput];
			state = ror64(state, 4); 
		}

	}
	
	state ^= rk[i];
	*(uint64_t*)cipherText = state;
}

*/
void present_64_128_test (u8 *plainText) {
	/*
	 * PRESENT 64/128
	 */
	
	// plain text: 3b726574 7475432d
	
	//val = rand()%256;
	

	

	int i, j;
	/*for ( i = 0; i < 16; i++ ) {
		for ( j = 0; j < 16; j++ ) {
			printf("%2x ", keys[i*16+j]);
		}
		printf("\n");
	}
	*/
	/*printf("PlainText: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
	
	/*printf("After encryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
	//present_decrypt(plainText, keys);
	/*printf("After decryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
}

int main () {
	FILE * fp = fopen("present_edit.csv", "w"); 
	
	clock_t t;
    struct timeval start, end;
  	
	long double totaltime , timearray[64000]; 
	long double timeavg;
	totaltime=0;
    
    for (int j = 0; j < 64000; ++j)
    {
	u8 inputKey[10] = {0x00};
	u8 keys[PRESENT_KEY_SIZE_128/8*(PRESENT_ROUNDS+1)];
	present_64_128_key_schedule(inputKey, keys);
	srand((unsigned) time(&t));
	u8 plainText[] = {rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256};
	
  	gettimeofday(&start, NULL);
	
	present_encrypt(plainText, keys);

	gettimeofday(&end, NULL);

  	//printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
  	timearray[j]=((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec));
  	fprintf(fp, "%.5LG\n", timearray[j]);
  	totaltime = totaltime + ((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec));
  	    }
  	
  	timeavg=totaltime/64000;

	printf("%LG\n", timeavg);
	printf("Standard Deviation = %.5lf\n", standard_deviation(timearray,64000,timeavg));
	fclose(fp);
	return 0;

}
double standard_deviation(long double timearray[], int n, long double timeavg)
{
    long double mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    sum_deviation+=(timearray[i]-timeavg)*(timearray[i]-timeavg);
    double sd=0.0,m=0.0;
    m=(sum_deviation/n);
    sd = sqrt(m);   
 	return (sd);             
}
