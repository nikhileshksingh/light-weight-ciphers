#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>

#define ITER 512000

/*
 *
 * The input is from right to left.
 * The rightmost byte is the least significant byte.
 * The leftmost byte is the most significant byte.
 */



#define SPECK_BLOCK_SIZE    (64)
#define SPECK_WORD_SIZE     (32)
#define SPECK_A             (8)
#define SPECK_B             (3)
//#define SPECK_KEY_WORDS_96  (3)
//#define SPECK_ROUNDS_96     (26)
#define SPECK_KEY_WORDS_128 (4)
#define SPECK_ROUNDS_128    (27)
typedef  uint8_t  u8;
typedef  uint16_t u16;
typedef  uint32_t u32;
typedef  uint64_t u64;

double standard_deviation(long double timearray[], int n, long double timeavg);
/*
 * rotate shift right n-bit on x(a 64-bit block)
 */
static inline uint64_t ror64(uint64_t x, int n) { return x>>n | x<<(64-n); }

/*
 * rotate shift left x by n bits
 */
static inline uint32_t rol32(uint32_t x, int n) { return x<<n | x>>(32-n); }

/*
 * rotate shift right x by n bits
 */
static inline uint32_t ror32(uint32_t x, int n) { return x>>n | x<<(32-n); }

/*
 * round function
 */
#define roundFunction(x, y, k) \
    x = ror32(x, SPECK_A);     \
    x += y;                    \
	x ^= k;                    \
	y = rol32(y, SPECK_B);     \
	y ^= x                     \

/*
 * invert round function
 */
/*
#define invertRoundFunction(x, y, k) \
	y ^= x;                          \
	y = ror32(y, SPECK_B);           \
	x ^= k;                          \
	x -= y;                          \
	x = rol32(x, SPECK_A)            \
*/
/*
 * key schedule
 * inputKey: the original keys
 * keys: round keys
 */
/*void speck_64_96_key_schedule(const u8 * inputKey, u8 * keys ) {

	u32 *rk = (u32*)keys;
	const u32 *ik = (const u32*)inputKey;

	u32 l[SPECK_ROUNDS_96 + SPECK_KEY_WORDS_96-2];
	rk[0] = ik[0];
	l[0] = ik[1];
	l[1] = ik[2];

	int i;
	for ( i = 0; i < SPECK_ROUNDS_96-1; i++ ) {
		l[i+SPECK_KEY_WORDS_96-1] = (rk[i] + ror32(l[i], SPECK_A)) ^ (u32)i;
		rk[i+1] = rol32(rk[i], SPECK_B) ^ l[i+SPECK_KEY_WORDS_96-1];
	}
}
*/
void speck_64_128_key_schedule(const u8 * inputKey, u8 * keys ) {

	u32 *rk = (u32*)keys;
	const u32 *ik = (const u32*)inputKey;

	u32 l[SPECK_ROUNDS_128 + SPECK_KEY_WORDS_128-2];
	rk[0] = ik[0];
	l[0] = ik[1];
	l[1] = ik[2];
	l[2] = ik[3];

	int i;
	for ( i = 0; i < SPECK_ROUNDS_128-1; i++ ) {
		l[i+SPECK_KEY_WORDS_128-1] = (rk[i] + ror32(l[i], SPECK_A)) ^ (u32)i;
		rk[i+1] = rol32(rk[i], SPECK_B) ^ l[i+SPECK_KEY_WORDS_128-1];
	}
}

/*
 * encrypt
 * plainText: plainText has just one block.
 * keys: round keys
 */
static void encrypt(u8 * plainText, const u8 * keys, int ROUNDS) {
	u32 *plain = (u32*)plainText;
	const u32* rk = (const u32*)keys;

	int i;
	for ( i = 0; i < ROUNDS; i++ ) {
		roundFunction(plain[1], plain[0], rk[i]);
	}
}

/*void speck_64_96_encrypt(u8 * plainText, const u8 * keys) {
    encrypt(plainText, keys, SPECK_ROUNDS_96);
}*/

void speck_64_128_encrypt(u8 * plainText, const u8 * keys) {
    encrypt(plainText, keys, SPECK_ROUNDS_128);
}

/*
 * decrypt
 * cipherText: cipherText has just one block.
 * keys: round keys
 */
/*
static void decrypt(u8 * cipherText, const u8 * keys, int ROUNDS) {
	u32 *cipher = (u32*)cipherText;
	const u32* rk = (const u32*)keys;

	int i;
	for ( i = ROUNDS-1; i >= 0; i-- ) {
		invertRoundFunction(cipher[1], cipher[0], rk[i]);
		//cipher[0] = ror32(cipher[1] ^ cipher[0], SPECK_B);
		//cipher[1] = rol32(((cipher[1] ^ rk[i]) - cipher[0]), SPECK_A);
	}
}
*/
/*void speck_64_96_decrypt(u8 * cipherText, const u8 * keys) {
    decrypt(cipherText, keys, SPECK_ROUNDS_96);
}*/
/*
void speck_64_128_decrypt(u8 * cipherText, const u8 * keys) {
    decrypt(cipherText, keys, SPECK_ROUNDS_128);
}
*/

/*
 * key schedule
 * inputKey: the original keys
 * keys: round keys
 */
//void speck_64_96_key_schedule(const u8 * inputKey, u8 * keys );

void speck_64_128_key_schedule(const u8 * inputKey, u8 * keys );

/*
 * encrypt
 * plainText: plainText has just one block.
 * keys: round keys
 */
//void speck_64_96_encrypt(u8 * plainText, const u8 * keys );

void speck_64_128_encrypt(u8 * plainText, const u8 * keys );

/*
 * decrypt
 * cipherText: cipherText has just one block.
 * keys: round keys
 */
//void speck_64_96_decrypt(u8 * cipherText, const u8 * keys );

//void speck_64_128_decrypt(u8 * cipherText, const u8 * keys );
/*
	 * Speck 64/96
	 */

/*void speck_64_96_test () {

	u8 inputKey[] = {0x00, 0x01, 0x02, 0x03, 0x08, 0x09, 0x0a, 0x0b, 0x10, 0x11, 0x12, 0x13};
	u8 keys[SPECK_BLOCK_SIZE/16*SPECK_ROUNDS_96];
	// plain text: 74614620 736e6165
	u8 plainText[] = {0x65, 0x61, 0x6e, 0x73, 0x20, 0x46, 0x61, 0x74};

	speck_64_96_key_schedule(inputKey, keys);

	printf("PlainText: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);

	speck_64_96_encrypt(plainText, keys);
	// cipher text: 9f7952ec 4175946c
	printf("After encryption: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);

	speck_64_96_decrypt(plainText, keys);
	printf("After decryption: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);
}
*/
void speck_64_128_test() {
	/*
	 * Speck 64/128
	 */

	/*printf("PlainText: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/

	// cipher text: 8c6fa548 454e028b

	/*printf("After encryption: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
	//speck_64_128_decrypt(plainText, keys);
	/*printf("After decryption: %x, %x, %x, %x, %x, %x, %x, %x\n",
		plainText[0], plainText[1], plainText[2], plainText[3],
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
}

int main () {

	//speck_64_96_test();
	FILE * fp = fopen("speck_edit.csv", "w");
	clock_t t;
  struct timespec start, end;

	long double totaltime , timearray[ITER];
	long double timeavg;
	totaltime=0;

    for (int j = 0; j < ITER; ++j)
    {	srand((unsigned) time(&t));
	u8 inputKey[] = {0x00, 0x01, 0x02, 0x03, 0x08, 0x09, 0x0a, 0x0b, 0x10, 0x11, 0x12, 0x13, 0x18, 0x19, 0x1a, 0x1b};
	u8 keys[SPECK_BLOCK_SIZE/16*SPECK_ROUNDS_128];
	int lower,upper;
	u8 plainText[] = {rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256};
	speck_64_128_key_schedule(inputKey, keys);



  clock_gettime(CLOCK_REALTIME, &start);

	speck_64_128_encrypt(plainText, keys);

	clock_gettime(CLOCK_REALTIME, &end);

  	//printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
  	timearray[j]=((end.tv_sec * 1000000000 + end.tv_nsec)- (start.tv_sec * 1000000000 + start.tv_nsec));
  	fprintf(fp, "%.5LG\n", timearray[j]);
  	totaltime = totaltime + ((end.tv_sec * 1000000000 + end.tv_nsec)- (start.tv_sec * 1000000000 + start.tv_nsec));
  	    }

  	timeavg=totaltime/ITER;

	printf("%LG\n", timeavg);
	printf("Standard Deviation = %.5lf\n", standard_deviation(timearray,ITER,timeavg));
	fclose(fp);
	return 0;
}

double standard_deviation(long double timearray[], int n, long double timeavg)
{
    long double mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    sum_deviation+=(timearray[i]-timeavg)*(timearray[i]-timeavg);
    double sd=0.0,m=0.0;
    m=(sum_deviation/n);
    sd = sqrt(m);
 	return (sd);
}
