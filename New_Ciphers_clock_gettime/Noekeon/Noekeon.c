/************************************************************************************/
/* Noekeontest.c
 *
 * Last Modified: 00/09/26             Created: 00/08/30
 *
 * Project    : Nessie Proposal: NOEKEON
 *
 * Authors    : Joan Daemen, Michael Peeters, Vincent Rijmen, Gilles Van Assche
 *
 * Written by : Michael Peeters
 *
 * References : [NESSIE] see http://cryptonessie.org/ for information about
 *                       interface conventions and definition of portable C.
 *
 * Description: Generate test vector for block cipher NOEKEON
 *
 ***********************************************************************************/

#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <stddef.h>
#include <time.h>
#include <sys/time.h>
#include <math.h>
#include <stdlib.h>

#include "NessieInterfaces.h"
#include "Nessie.h"

#define ITER 512000


#define RC1ENCRYPTSTART  T8 (0x80)
#define RC2DECRYPTSTART  T8 (0xD4)

#define NROUND 16

/*==================================================================================*/
/* Null Vector
/*----------------------------------------------------------------------------------*/
u32 NullVector[4] = {0,0,0,0};


/*==================================================================================*/
void Theta (u32 const * const k,u32 * const a)
/*----------------------------------------------------------------------------------*/
/* DIFFUSION - Linear step THETA, involution
/*==================================================================================*/
{
  u32 tmp;

  tmp  = a[0]^a[2];
  tmp ^= ROTL32(tmp,8)^ROTL32(tmp,24);
  a[1]^= tmp;
  a[3]^= tmp;

  a[0] ^= k[0]; a[1] ^= k[1]; a[2] ^= k[2]; a[3] ^= k[3];

  tmp  = a[1]^a[3];
  tmp ^= ROTL32(tmp,8)^ROTL32(tmp,24);
  a[0]^= tmp;
  a[2]^= tmp;

} /* Theta */


/*==================================================================================*/
void Pi1(u32 * const a)
/*----------------------------------------------------------------------------------*/
/* DISPERSION - Rotations Pi1
/*==================================================================================*/
{ a[1] = ROTL32 (a[1], 1);
  a[2] = ROTL32 (a[2], 5);
  a[3] = ROTL32 (a[3], 2);
}  /* Pi1 */


/*==================================================================================*/
void Pi2(u32 * const a)
/*----------------------------------------------------------------------------------*/
/* DISPERSION - Rotations Pi2
/*==================================================================================*/
{ a[1] = ROTL32 (a[1], 31);
  a[2] = ROTL32 (a[2], 27);
  a[3] = ROTL32 (a[3], 30);
}  /* Pi2 */


/*==================================================================================*/
void Gamma(u32 * const a)
/*----------------------------------------------------------------------------------*/
/* NONLINEAR - gamma, involution
/*----------------------------------------------------------------------------------*/
/* Input of i_th s-box = (i3)(i2)(i1)(i0), with (i3) = i_th bit of a[3]
 *                                              (i2) = i_th bit of a[2]
 *                                              (i1) = i_th bit of a[1]
 *                                              (i0) = i_th bit of a[0]
 *
 * gamma = NLIN o LIN o NLIN : (i3)(i2)(i1)(i0) --> (o3)(o2)(o1)(o0)
 *
 * NLIN ((i3) = (o3) = (i3)                     NLIN is an involution
 *       (i2)   (o2)   (i2)                      i.e. evaluation order of i1 & i0
 *       (i1)   (o1)   (i1+(~i3.~i2))                 can be swapped
 *       (i0))  (o0)   (i0+(i2.i1))
 *
 *  LIN ((i3) = (o3) = (0.i3+0.i2+0.i1+  i0)    LIN is an involution
 *       (i2)   (o2)   (  i3+  i2+  i1+  i0)
 *       (i1)   (o1)   (0.i3+0.i2+  i1+0.i0)
 *       (i0))  (o0)   (  i3+0.i2+0.i1+0.i0)
 *
/*==================================================================================*/
{ u32 tmp;

  /* first non-linear step in gamma */
  a[1] ^= ~a[3] & ~a[2];
  a[0] ^=   a[2] & a[1];

  /* linear step in gamma */
  tmp   = a[3];
  a[3]  = a[0];
  a[0]  = tmp;
  a[2] ^= a[0]^a[1]^a[3];

  /* last non-linear step in gamma */
  a[1] ^= ~a[3] & ~a[2];
  a[0] ^=   a[2] & a[1];
} /* Gamma */


/*==================================================================================*/
void Round (u32 const * const k,u32 * const a,u8 const RC1,u8 const RC2)
/*----------------------------------------------------------------------------------*/
/* The round function, common to both encryption and decryption
/* - Round constants is added to the rightmost byte of the leftmost 32-bit word (=a0)
/*==================================================================================*/
{
  a[0] ^= RC1;
  Theta(k,a);
  a[0] ^= RC2;
  Pi1(a);
  Gamma(a);
  Pi2(a);
}  /* Round */

/*==================================================================================*/
void RCShiftRegFwd (u8 * const RC)
/*----------------------------------------------------------------------------------*/
/* The shift register that computes round constants - Forward Shift
/*==================================================================================*/
{

  if ((*RC)&0x80) (*RC)=((*RC)<<1) ^ 0x1B; else (*RC)<<=1;

} /* RCShiftRegFwd */

/*==================================================================================*/
void RCShiftRegBwd (u8 * const RC)
/*----------------------------------------------------------------------------------*/
/* The shift register that computes round constants - Backward Shift
/*==================================================================================*/
{

  if ((*RC)&0x01) (*RC)=((*RC)>>1) ^ 0x8D; else (*RC)>>=1;

} /* RCShiftRegBwd */

/*==================================================================================*/
void CommonLoop (u32 const * const k,u32 * const a, u8 RC1, u8 RC2)
/*----------------------------------------------------------------------------------*/
/* loop - several round functions, ended by theta
/*==================================================================================*/
{
  unsigned i;

  for(i=0 ; i<NROUND ; i++) {
    Round(k,a,RC1,RC2);
    RCShiftRegFwd(&RC1);
    RCShiftRegBwd(&RC2);
  }
  a[0]^=RC1;
  Theta(k,a);
  a[0]^=RC2;

} /* CommonLoop */



/*==================================================================================*/
void NESSIEkeysetup(const unsigned char * const key,
                    struct NESSIEstruct * const structpointer)
/*----------------------------------------------------------------------------------*/
/* PRE:
 * 128-bit key value in byte array key [16 bytes]
 *
 * key: [00] [01] [02] [03] [04] [05] [06] [07] [08] [09] [10] [11] [12] [13] [14] [15]
 *      ----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----
 *
 * POST:
 * key value written in 32-bit word array k in NESSIEstruct
 *      -------------------+-------------------+-------------------+-------------------
 *              k[0]                k[1]                k[2]                k[3]
/*==================================================================================*/
{ u32 *k=structpointer->k;

  k[0]=U8TO32_BIG(key   );
  k[1]=U8TO32_BIG(key+4 );
  k[2]=U8TO32_BIG(key+8 );
  k[3]=U8TO32_BIG(key+12);

} /* NESSIEkeysetup */


/*==================================================================================*/
void NESSIEencrypt(const struct NESSIEstruct * const structpointer,
                   const unsigned char * const plaintext,
                   unsigned char * const ciphertext)
/*==================================================================================*/
{ u32 const *k=structpointer->k;
  u32 state[4];


  state[0]=U8TO32_BIG(plaintext   );
  state[1]=U8TO32_BIG(plaintext+4 );
  state[2]=U8TO32_BIG(plaintext+8 );
  state[3]=U8TO32_BIG(plaintext+12);

  CommonLoop (k,state,RC1ENCRYPTSTART,0);

  U32TO8_BIG(ciphertext   , state[0]);
  U32TO8_BIG(ciphertext+4 , state[1]);
  U32TO8_BIG(ciphertext+8 , state[2]);
  U32TO8_BIG(ciphertext+12, state[3]);
} /* NESSIEencrypt */


/*==================================================================================*/
void NESSIEdecrypt(const struct NESSIEstruct * const structpointer,
                   const unsigned char * const ciphertext,
                   unsigned char * const plaintext)
/*==================================================================================*/
{ u32 const *kencrypt=structpointer->k;
  u32 k[4],state[4];

  state[0]=U8TO32_BIG(ciphertext   );
  state[1]=U8TO32_BIG(ciphertext+4 );
  state[2]=U8TO32_BIG(ciphertext+8 );
  state[3]=U8TO32_BIG(ciphertext+12);

  k[0]=kencrypt[0];
  k[1]=kencrypt[1];
  k[2]=kencrypt[2];
  k[3]=kencrypt[3];
  Theta(NullVector,k);

  CommonLoop (k,state,0,RC2DECRYPTSTART);

  U32TO8_BIG(plaintext   , state[0]);
  U32TO8_BIG(plaintext+4 , state[1]);
  U32TO8_BIG(plaintext+8 , state[2]);
  U32TO8_BIG(plaintext+12, state[3]);
} /* NESSIEdecrypt */



char *printVector8 (char * const s, u8 const * const v)
{
  sprintf (s,"%02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x %02x%02x%02x%02x",
           v[0],v[1],v[2],v[3],v[4],v[5],v[6],v[7],v[8],v[9],v[10],v[11],v[12],v[13],v[14],v[15]);
  return s;
}

char *printVector32 (char * const s, u32 const * const v)
{
  sprintf (s,"%08x %08x %08x %08x",v[0],v[1],v[2],v[3]);
  return s;
}


double standard_deviation(long double timearray[], int n, long double timeavg)
{
    long double mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    sum_deviation+=(timearray[i]-timeavg)*(timearray[i]-timeavg);
    double sd=0.0,m=0.0;
    m=(sum_deviation/n);
    sd = sqrt(m);
 	return (sd);
}

int main (int argc, char *argv[])
{

  FILE * fp = fopen("noekeon.csv", "w");
	clock_t t;
	struct timespec start, end;

	long double totaltime , timearray[ITER];
	long double timeavg;
	totaltime=0;

  u8 *k;
  u8 k0[16] = {0x01,0x02,0x03,0x04,0x05,0x06,0x07,0x08,0x09,0x00,0x0a,0x0b,0x0c,0x0d,0x0e,0x0f};
  //u8 k1[16] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
  //u8 k2sched [16] = {0xba,0x69,0x33,0x81,0x92,0x99,0xc7,0x16,0x99,0xa9,0x9f,0x08,0xf6,0x78,0x17,0x8b};
  //u8 k2direct[16] = {0xb1,0x65,0x68,0x51,0x69,0x9e,0x29,0xfa,0x24,0xb7,0x01,0x48,0x50,0x3d,0x2d,0xfc};
  k=k0;


  srand(time(NULL));

  u8 *a;

  //u8 a0[16] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
  //u8 a1[16] = {0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff};
  //u8 a2sched [16] = {0x52,0xf8,0x8a,0x7b,0x28,0x3c,0x1f,0x7b,0xdf,0x7b,0x6f,0xaa,0x50,0x11,0xc7,0xd8};
  //u8 a2direct[16] = {0x2a,0x78,0x42,0x1b,0x87,0xc7,0xd0,0x92,0x4f,0x26,0x11,0x3f,0x1d,0x13,0x49,0xb2};

  u8 b[16];
  u8 c[16];
  struct NESSIEstruct keystruct;
  char outs[200];

  //printf ("                     k = %s\n",printVector8(outs,k));
  //printf ("                     a = %s\n",printVector8(outs,a));

  for(int j = 0; j < ITER; ++j)
  {
    u8 a0[16] = {rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,
                 rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,};

    a=a0;

    NESSIEkeysetup (k,&keystruct);

    clock_gettime(CLOCK_REALTIME, &start);

    NESSIEencrypt (&keystruct,a,b);

    clock_gettime(CLOCK_REALTIME, &end);

    timearray[j]=((end.tv_sec * 1000000000 + end.tv_nsec)- (start.tv_sec * 1000000000 + start.tv_nsec));
    fprintf(fp, "%.5LG\n", timearray[j]);
    totaltime = totaltime + ((end.tv_sec * 1000000000 + end.tv_nsec)- (start.tv_sec * 1000000000 + start.tv_nsec));
  }

  timeavg = totaltime / ITER;







  //printf ("after NESSIEencrypt, b = %s\n",printVector8(outs,b));
  //NESSIEdecrypt (&keystruct,b,c);
  //printf ("after NESSIEdecrypt, a?= %s\n",printVector8(outs,c));
  //printf ("\n");

  printf("%LG\n", timeavg);
  printf("Standard Deviation = %.5lf\n", standard_deviation(timearray,ITER,timeavg));
  fclose(fp);
	exit(0);

  return 0;
}
