echo "---------------------------------------------"
echo "---------------------------------------------"
echo "Tasks in Core 0:"

echo "AES:"
cd AES/
taskset -c 0 ./aes_edit
cd ..
echo "---------------------------------"

echo "Clefia:"
cd Clefia/
taskset -c 0 ./Clefia
cd ..
echo "---------------------------------"

echo "KATAN:"
cd KATAN/
taskset -c 0 ./katan64
cd ..
echo "---------------------------------"

echo "KLEIN:"
cd KLEIN/
taskset -c 0 ./KLEIN
cd ..
echo "---------------------------------"

echo "MCRYPTON128:"
cd MCRYPTON128/
taskset -c 0 ./mCrypton128
cd ..
echo "---------------------------------"

echo "Noekeon:"
cd Noekeon/
taskset -c 0 ./Noekeon
cd ..
echo "---------------------------------"

echo "Piccolo128:"
cd Piccolo128/
taskset -c 0 ./piccolo128
cd ..
echo "---------------------------------"

echo "PRESENT:"
cd PRESENT/
taskset -c 0 ./present_edit
cd ..
echo "---------------------------------"

echo "PRINCE:"
cd PRINCE/
taskset -c 0 ./prince_c_ref
cd ..
echo "---------------------------------"

echo "SIMON:"
cd SIMON/
taskset -c 0 ./simon_edit
cd ..
echo "---------------------------------"

echo "SPECK:"
cd SPECK/
taskset -c 0 ./speck_edit
cd ..
echo "---------------------------------"


echo "---------------------------------------------"
echo "---------------------------------------------"
echo "Tasks in Core 2:"

echo "AES:"
cd AES/
taskset -c 2 ./aes_edit
cd ..
echo "---------------------------------"

echo "Clefia:"
cd Clefia/
taskset -c 2 ./Clefia
cd ..
echo "---------------------------------"

echo "KATAN:"
cd KATAN/
taskset -c 2 ./katan64
cd ..
echo "---------------------------------"

echo "KLEIN:"
cd KLEIN/
taskset -c 2 ./KLEIN
cd ..
echo "---------------------------------"

echo "MCRYPTON128:"
cd MCRYPTON128/
taskset -c 2 ./mCrypton128
cd ..
echo "---------------------------------"

echo "Noekeon:"
cd Noekeon/
taskset -c 2 ./Noekeon
cd ..
echo "---------------------------------"

echo "Piccolo128:"
cd Piccolo128/
taskset -c 2 ./piccolo128
cd ..
echo "---------------------------------"

echo "PRESENT:"
cd PRESENT/
taskset -c 2 ./present_edit
cd ..
echo "---------------------------------"

echo "PRINCE:"
cd PRINCE/
taskset -c 2 ./prince_c_ref
cd ..
echo "---------------------------------"

echo "SIMON:"
cd SIMON/
taskset -c 2 ./simon_edit
cd ..
echo "---------------------------------"

echo "SPECK:"
cd SPECK/
taskset -c 2 ./speck_edit
cd ..
echo "---------------------------------"




