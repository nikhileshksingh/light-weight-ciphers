#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>
int main(void)
{
        const size_t num_syscalls = 1000000000;
        struct timeval begin, end, now;
        struct timespec start, finish, n;
	//time_t now;
        long long i;

        //gettimeofday(&begin, NULL);
        clock_gettime(CLOCK_REALTIME, &start);
        for (i = 0; i < num_syscalls; ++i) {
		//methods:

                gettimeofday(&now, NULL);
                //clock_gettime(CLOCK_MONOTONIC, &n);
		//clock_gettime(CLOCK_REALTIME, &n);
		//clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &n);
		//time(&now);
        }
        //gettimeofday(&end, NULL);
        clock_gettime(CLOCK_REALTIME, &finish);

        //printf("time = %u.%06u\n", begin.tv_sec, begin.tv_usec);
        //printf("time = %u.%06u\n", end.tv_sec, end.tv_usec);
        //printf("time diff = %u.%06u\n", end.tv_sec - begin.tv_sec, end.tv_usec - begin.tv_usec);

        printf("time = %u.%06u\n", start.tv_sec, start.tv_nsec);
        printf("time = %u.%06u\n", finish.tv_sec, finish.tv_nsec);
        printf("time diff = %u.%06u\n", finish.tv_sec - start.tv_sec, finish.tv_nsec - start.tv_nsec);



        printf("Number of Calls = %u\n", num_syscalls);


}
