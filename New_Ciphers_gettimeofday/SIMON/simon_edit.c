#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <stdint.h>
#include <time.h>
#include <sys/time.h>
/*
 *
 * The input is from right to left. 
 * The rightmost byte is the least significant byte.
 * The leftmost byte is the most significant byte.
 */



#define SIMON_BLOCK_SIZE          (64)
#define SIMON_WORD_SIZE           (32)
#define SIMON_CONST_C             (0xfffffffc)
//#define SIMON_KEY_WORDS_96        (3)
//#define SIMON_ROUNDS_96           (42)
#define SIMON_KEY_WORDS_128       (4)
#define SIMON_ROUNDS_128          (44)
typedef  uint8_t  u8;
typedef  uint16_t u16;
typedef  uint32_t u32;
typedef  uint64_t u64;

/*
 * key schedule
 * inputKey: the original keys
 * keys: round keys
 */
double standard_deviation(long double timearray[], int n, long double timeavg);

/*
 * rotate shift right n-bit on x(a 64-bit block)
 */
static inline uint64_t ror64(uint64_t x, int n) { return x>>n | x<<(64-n); }

/*
 * rotate shift left x by n bits
 */
static inline uint32_t rol32(uint32_t x, int n) { return x<<n | x>>(32-n); }

/*
 * rotate shift right x by n bits
 */
static inline uint32_t ror32(uint32_t x, int n) { return x>>n | x<<(32-n); }
//void simon_64_96_key_schedule(const u8 * inputKey, u8 * keys );

void simon_64_128_key_schedule(const u8 * inputKey, u8 * keys );

/*
 * encrypt
 * plainText: plainText has just one block.
 * keys: round keys
 */
//void simon_64_96_encrypt(u8 * plainText, const u8 * keys );

void simon_64_128_encrypt(u8 * plainText, const u8 * keys );

/*
 * decrypt
 * cipherText: cipherText has just one block.
 * keys: round keys
 */
//void simon_64_96_decrypt(u8 * cipherText, const u8 * keys );

//void simon_64_128_decrypt(u8 * cipherText, const u8 * keys );

/*
 * function f
 */
#define f(x) ((rol32(x, 1)&rol32(x, 8)) ^ rol32(x, 2))

/*
 * const z
 * can only be used in this file
 */
static const u8 z2[64] = 
    {1,0,1,0,1,1,1,1,0,1,1,1,0,0,0,0,0,0,1,1,0,1,0,0,1,0,0,1,1,0,0,0,1,0,1,0,0,0,0,1,0,0,0,1,1,1,1,1,1,0,0,1,0,1,1,0,1,1,0,0,1,1};
static const u8 z3[64] = 
    {1,1,0,1,1,0,1,1,1,0,1,0,1,1,0,0,0,1,1,0,0,1,0,1,1,1,1,0,0,0,0,0,0,1,0,0,1,0,0,0,1,0,1,0,0,1,1,1,0,0,1,1,0,1,0,0,0,0,1,1,1,1};

/*
 * key schedule
 * inputKey: the original keys
 * keys: round keys
 */
/*void simon_64_96_key_schedule(const u8 * inputKey, u8 * keys ) {

	const u32 *ik = (const u32*)inputKey;
	u32 *rk = (u32*)keys;

	int i;
	for ( i = 0; i < SIMON_KEY_WORDS_96; ++i )  {
		rk[i] = ik[i];
	}

	u32 temp;
	for ( i = SIMON_KEY_WORDS_96; i < SIMON_ROUNDS_96; ++i ) {
		temp = ror32(rk[i-1], 3);
		temp ^= ror32(temp, 1);
		rk[i] = SIMON_CONST_C ^ rk[i-SIMON_KEY_WORDS_96] ^ temp;
		if ( z2[(i-SIMON_KEY_WORDS_96)%62] == 1 ) {
			rk[i] ^=  0x1;
		}
	}
}
*/
void simon_64_128_key_schedule(const u8 * inputKey, u8 * keys ) {

	const u32 *ik = (const u32*)inputKey;
	u32 *rk = (u32*)keys;

	int i;
	for ( i = 0; i < SIMON_KEY_WORDS_128; ++i )  {
		rk[i] = ik[i];
	}

	u32 temp;
	for ( i = SIMON_KEY_WORDS_128; i < SIMON_ROUNDS_128; ++i ) {
		temp = ror32(rk[i-1], 3);
		temp ^= rk[i-3];
		temp ^= ror32(temp, 1);
		rk[i] = SIMON_CONST_C ^ rk[i-SIMON_KEY_WORDS_128] ^ temp;
		if ( z3[(i-SIMON_KEY_WORDS_128)%62] == 1 ) {
			rk[i] ^=  0x1;
		}
	}
}

/*
 * encrypt
 * plainText: plainText has just one block.
 * keys: round keys
 */
static void encrypt(u8 * plainText, const u8 * keys, int ROUNDS) {

	u32 *plain = (u32*)plainText;
	const u32 *rk = (const u32*)keys;

	int i;
	for ( i = 0; i < ROUNDS; i+=2 ) {
		plain[0] = plain[0] ^ rk[i] ^ f(plain[1]);
		plain[1] = plain[1] ^ rk[i+1] ^ f(plain[0]);
	}
}

/*void simon_64_96_encrypt(u8 * plainText, const u8 * keys) {
    encrypt(plainText, keys, SIMON_ROUNDS_96);
}*/

void simon_64_128_encrypt(u8 * plainText, const u8 * keys) {
    encrypt(plainText, keys, SIMON_ROUNDS_128);
}

/*
 * decrypt
 * cipherText: cipherText has just one block.
 * keys: round keys
 */
/*
static void decrypt(u8 * cipherText, const u8 * keys, int ROUNDS) {

	u32 *cipher = (u32*)cipherText;
	const u32 *rk = (const u32*)keys;    
	
	int i;
	for ( i = ROUNDS-1; i >= 0; i-=2 ) {
		cipher[1] = cipher[1] ^ rk[i] ^ f(cipher[0]);
		cipher[0] = cipher[0] ^ rk[i-1] ^ f(cipher[1]);
	}
}

void simon_64_96_decrypt(u8 * cipherText, const u8 * keys) {
    decrypt(cipherText, keys, SIMON_ROUNDS_96);
}

*/



/*void simon_64_128_decrypt(u8 * cipherText, const u8 * keys) {
    decrypt(cipherText, keys, SIMON_ROUNDS_128);
}
*/
/*
	 Simon64/96
	 */
/*void simon_64_96_test() {
	
	u8 inputKey[] = {0x00, 0x01, 0x02, 0x03, 0x08, 0x09, 0x0a, 0x0b, 0x10, 0x11, 0x12, 0x13};
	u8 keys[SIMON_BLOCK_SIZE/16*SIMON_ROUNDS_96];
	u8 plainText[] = {0x63, 0x6c, 0x69, 0x6e, 0x67, 0x20, 0x72, 0x6f};

	simon_64_96_key_schedule(inputKey, keys);

	printf("PlainText: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);

	simon_64_96_encrypt(plainText, keys);
	// cipherText is: 0x5ca2e27f 0x111a8fc8
	printf("After encryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	
	simon_64_96_decrypt(plainText, keys);
	printf("After decryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);

}
*/

/*
	 * Simon64/128
	 */
void simon_64_128_test() {
	
	

	/*printf("PlainText: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
	
	//simon_64_128_decrypt(plainText, keys);
	/*printf("After decryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/
}

int main () {
		

	FILE * fp = fopen("simon_edit.csv", "w"); 
	clock_t t;
    	struct timeval start, end;

	//struct timespec start, end; 
  	
	long double totaltime , timearray[64000]; 
	long double timeavg;
	totaltime=0;
    
    for (int j = 0; j < 64000; ++j)
    {	srand((unsigned) time(&t));
	u8 inputKey[] = {0x00, 0x01, 0x02, 0x03, 0x08, 0x09, 0x0a, 0x0b, 0x10, 0x11, 0x12, 0x13, 0x18, 0x19, 0x1a, 0x1b};
	u8 keys[SIMON_BLOCK_SIZE/16*SIMON_ROUNDS_128];
	int lower,upper;
	u8 plainText[] = {rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256,rand()%256};
	/*printf("Before encryption: %02x, %02x, %02x, %02x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 
		plainText[4], plainText[5], plainText[6], plainText[7]);
	*/	
	simon_64_128_key_schedule(inputKey, keys);

  	gettimeofday(&start, NULL);
	
	//clock_gettime(CLOCK_MONOTONIC, &start); 

	simon_64_128_encrypt(plainText, keys);
	gettimeofday(&end, NULL);

	//clock_gettime(CLOCK_MONOTONIC, &end); 

/*	printf("After encryption: %x, %x, %x, %x, %x, %x, %x, %x\n", 
		plainText[0], plainText[1], plainText[2], plainText[3], 

		plainText[4], plainText[5], plainText[6], plainText[7]);
	
*/	
  	//printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec)));
  	timearray[j]=((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec));
  	fprintf(fp, "%.5LG\n", timearray[j]);
  	totaltime = totaltime + ((end.tv_sec * 1000000 + end.tv_usec)- (start.tv_sec * 1000000 + start.tv_usec));
  	    }
  	
  	timeavg=totaltime/64000.0;

	printf("%LG\n", timeavg);
	printf("Standard Deviation = %.5lf\n", standard_deviation(timearray,64000,timeavg));
	fclose(fp);
	return 0;

}
double standard_deviation(long double timearray[], int n, long double timeavg)
{
    long double mean=0.0, sum_deviation=0.0;
    int i;
    for(i=0; i<n;++i)
    sum_deviation+=(timearray[i]-timeavg)*(timearray[i]-timeavg);
    double sd=0.0,m=0.0;
    m=(sum_deviation/n);
    sd = sqrt(m);   
 	return (sd);             
}
