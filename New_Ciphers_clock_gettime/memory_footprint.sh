echo "---------------------------------------------"
echo "---------------------------------------------"
echo "Size:"

echo "AES:"
cd AES/
size aes_edit
cd ..
echo "---------------------------------"

echo "Clefia:"
cd Clefia/
size Clefia
cd ..
echo "---------------------------------"

echo "KATAN:"
cd KATAN/
size katan64
cd ..
echo "---------------------------------"

echo "KLEIN:"
cd KLEIN/
size KLEIN
cd ..
echo "---------------------------------"

echo "MCRYPTON128:"
cd MCRYPTON128/
size mCrypton128
cd ..
echo "---------------------------------"

echo "Noekeon:"
cd Noekeon/
size Noekeon
cd ..
echo "---------------------------------"

echo "Piccolo128:"
cd Piccolo128/
size piccolo128
cd ..
echo "---------------------------------"

echo "PRESENT:"
cd PRESENT/
size present_edit
cd ..
echo "---------------------------------"

echo "PRINCE:"
cd PRINCE/
size prince_c_ref
cd ..
echo "---------------------------------"

echo "SIMON:"
cd SIMON/
size simon_edit
cd ..
echo "---------------------------------"

echo "SPECK:"
cd SPECK/
size speck_edit
cd ..
echo "---------------------------------"



